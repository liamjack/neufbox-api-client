<?php

namespace LiamJack;

use GuzzleHttp\Client;

class NeufboxApiClient
{
    private $token;
    private $client;

    public function __construct($baseUri = "http://192.168.1.1/api/", $timeout = 5) {
        $this->client = new Client([
            'base_uri' => $baseUri,
            'timeout'  => $timeout,
        ]);
    }

    private function performRequest($apiMethod, $requestMethod = "GET", $parameters = []) {
        if (!in_array($requestMethod, array("GET", "POST")))
            throw new Exception("Invalid HTTP request method.");

        if (!is_array($parameters))
            throw new Exception('$parameters must be an array');

        $parameters['method'] = $apiMethod;

        if (isset($this->token))
            $parameters['token'] = $this->token;

        $response = $this->client->request($requestMethod, '', [
            "query" => $parameters
        ]);

        $xml = simplexml_load_string($response->getBody());

        if ($xml === FALSE)
            throw new Exception("Invalid XML returned.");

        if ($xml['stat']->__toString() != "ok")
            throw new Exception("Request failed.");

        return $xml;
    }

    private function generateHash($token, $username, $password) {
        $usernameHash = hash_hmac("sha256", hash("sha256", $username), $token);
        $passwordHash = hash_hmac("sha256", hash("sha256", $password), $token);

        return $usernameHash . $passwordHash;
    }

    /*
     * Authentication
     */

    public function getAuthToken() {
        $xml = $this->performRequest("auth.getToken");

        $this->token = $xml->auth['token']->__toString();

        return $this->token;
    }

    public function checkAuthToken($username = "admin", $password = "admin") {
        $this->performRequest("auth.checkToken", "GET", [
            'hash' => $this->generateHash($this->token, $username, $password)
        ]);
    }

    /*
     * DDNS
     */

    public function getDdnsInfo() {
        return current($this->performRequest("ddns.getInfo")->ddns->attributes());
    }

    public function enableDdns() {
        $this->performRequest("ddns.enable", "POST");
    }

    public function disableDdns() {
        $this->performRequest("ddns.disable", "POST");
    }

    public function forceDdnsUpdate() {
        $this->performRequest("ddns.forceDdnsUpdate", "POST");
    }

    /*
     * DSL
     */

    public function getDslInfo() {
        return current($this->performRequest("dsl.getInfo")->dsl->attributes());
    }

    /*
     * WLAN
     */

    public function getWlanInfo() {
        $xml = $this->performRequest("wlan.getInfo")->wlan;

        $array = current($xml->attributes());
        $array['wl0'] = current($xml->wl0->attributes());

        return $array;
    }

    public function getWlanClientList() {
        $xml = $this->performRequest("wlan.getClientList");

        $clients = [];

        foreach ($xml->client as $client)
            $clients[] = current($client->attributes());

        return $clients;
    }

    public function enableWlan() {
        $this->performRequest("wlan.enable", "POST");
    }

    public function disableWlan() {
        $this->performRequest("wlan.disable", "POST");
    }

    public function startWlan() {
        $this->performRequest("wlan.start", "POST");
    }

    public function stopWlan() {
        $this->performRequest("wlan.stop", "POST");
    }

    public function restartWlan() {
        $this->performRequest("wlan.restart", "POST");
    }

    /*
     * System
     */

    public function getSystemInfo() {
        return current($this->performRequest("system.getInfo")->system->attributes());
    }

    public function rebootSystem() {
        $this->performRequest("system.reboot", "POST");
    }

    /*
     * VOIP
     */

    public function getVoipInfo() {
        return current($this->performRequest("voip.getInfo")->voip->attributes());
    }

    public function startVoip() {
        $this->performRequest("voip.start", "POST");
    }

    public function stopVoip() {
        $this->performRequest("voip.stop", "POST");
    }

    public function restartVoip() {
        $this->performRequest("voip.restart", "POST");
    }

    /*
     * WAN
     */

    public function getWanInfo() {
        return current($this->performRequest("wan.getInfo")->wan->attributes());
    }

    /*
     * Firewall
     */

    public function getFirewallInfo() {
        return current($this->performRequest("firewall.getInfo")->firewall->attributes());
    }

    /*
     * LAN
     */

    public function getLanInfo() {
        return current($this->performRequest("lan.getInfo")->lan->attributes());
    }

    public function getLanHostList() {
        $xml = $this->performRequest("lan.getHostsList");

        $hosts = [];

        foreach ($xml->host as $host)
            $hosts[] = current($host->attributes());

        return $hosts;
    }

    /*
     * PPP
     */

    public function getPppInfo() {
        return current($this->performRequest("ppp.getInfo")->ppp->attributes());

    }
}